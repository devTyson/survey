package com.alignstech.survey.rest;

import com.alignstech.survey.model.ServerResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Headers;
import retrofit2.http.POST;


/**
 * Created by tyson on 9/12/17.
 */

public interface ApiInterface {
//
//    @Headers("Content-Type: application/x-www-form-urlencoded") <= yesko sato @FormUrlEncoded vanne lekheko,
// raw json pathauda matra yesari use garey huncha

    @FormUrlEncoded
    @POST("login")
    Call<ServerResponse> postRegister(@Field("username") String username, @Field("password") String userpassword);
}
