package com.alignstech.survey.view.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.alignstech.survey.R;
import com.alignstech.survey.model.ServerResponse;
import com.alignstech.survey.rest.ApiClient;
import com.alignstech.survey.rest.ApiInterface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    EditText et_userName,et_userPassword;
    Button btn_login;

    String username,password;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //initializing
        btn_login= (Button) findViewById(R.id.btn_login);
        et_userName= (EditText) findViewById(R.id.et_userName);
        et_userPassword = (EditText) findViewById(R.id.et_userPassword);



        //set onclick listener
        btn_login.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                //get the text from edittext after button click(yo ma jahile mathi nai garera birsinchu tehivayera lekhideko :D
                username = et_userName.getText().toString();
                password = et_userPassword.getText().toString();

                //Retrofit initialization gareko ..
                ApiInterface apiService =
                        ApiClient.getClient().create(ApiInterface.class);
                //yo <ServerResponse> gareko chen after success post garepachi ko response lai
                // ServerResponse classma directly serialization gareko ho..GSON use garnu ko faida
                Call<ServerResponse> call = apiService.postRegister(username,password);
                call.enqueue(new Callback<ServerResponse>() {
                    @Override
                    public void onResponse(Call<ServerResponse> call, Response<ServerResponse> response) {
                        if(response.body().getStatus()){
                            Toast.makeText(MainActivity.this,"Login Successfull",Toast.LENGTH_SHORT).show();
                        }
                        Log.d("Response",response.body().getMessage());
                    }

                    @Override
                    public void onFailure(Call<ServerResponse> call, Throwable t) {

                    }
                });
            }
        });


    }
}
/* timile content-type: application/json gareko thiyeu ni apiInterfacema tyo vaneko you were trying to send raw data
.i.e raw json. but postmanma try garda form select garera field pathako thiyeu haina?? raw select gareko thiyenauni..
so it is also one problem.but @field vanera pathaye pachi tyo content-type or accept vanera nalekhda pani lincha
ani ServerResponse ko model herera tarsira holau,, you don't need to worry about that..
postmanma jasto response aaucha teslai copy garera =>http://www.jsonschema2pojo.org/ sitema gayra response lai paste garne..
tesma package name, class name halne,, ani json select garne,ani gson select garne ani inclucde getter and setter select garne
then tala preview vanne button cha tyo click garera afno projectma same name ko model class banaudai copy paste gareni huncha,
natra zip download garera teslai directly projectma haldine.ani response thyakka objectma convert vayera bascha*/

//Project lai package banayera kaam gara..timro project herera ma ekchin tolaunu paryo :D :D start working in mvc or mvp
//MVC bujheko chau hola ,but MVP chen maile pani bujna sakeko chuina :D

